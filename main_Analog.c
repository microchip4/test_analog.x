/*
 * File:        main.c
 * Author:      Brandon Ruiz Vasquez 373666
 * Referencia:  http://www.ermicro.com/blog/?p=1408
 * Created on August 31, 2016, 7:18 PM
 */


#include <xc.h>
#include "Alteri.h"

void main(void) {
    TRISA=0;
    TRISD=0;
    ADCON0=0x01;
    // pg. 265
    // bit 5-2 CHS3:CHS0: Analog Channel Select 
    // AN0 is selected in this case Located in RA0
    // bit 0 ADON: A/D On bit
    // 1 = A/D converter module is enabled
    ADCON1=0x0E;
    /* pg. 266
     * bit 3-0 PCFG3:PCFG0: A/D Port Configuration Control bits:
     * in this case AN12 - AN1 are now Digital and only AN0 is analog
     */
    ADCON2=0x80;
    /* pg. 267
     * bit 7 ADFM: A/D Result Format Select bit
       1 = Right justified
     *              ADRESH                                   
     * | 0 | 0 | 0 | 0 | 0 | 0 |MSB|   |
     *              ADRESL
     * |   |   |   |   |   |   |   |LSB|      
       0 = Left justified
     *              ADRESH                                   
     * |MSB|   |   |   |   |   |   |   |
     *              ADRESL
     * |   |LSB| 0 | 0 | 0 | 0 | 0 | 0 |      
     */
    while(1){
        ADCON0bits.GO=1;
        while(ADCON0bits.GO);
            PORTD=ADRESL;
    }
    return;
}
